#!/bin/sh

# Useful Information in case of a problem:
echo "System: `/bin/uname -a`"
echo "Current working directory: `/bin/pwd`"
/bin/ls -ld .
echo "SAME_SENSOR_HOME = ${SAME_SENSOR_HOME}"
/bin/ls -ld ${SAME_SENSOR_HOME}
echo "PATH = ${PATH}"
/bin/ls -l /cvmfs/cms.cern.ch/SITECONF/local || true

# some known singularity locations
for LOCATION in \
         /util/opt/singularity/2.2.1/gcc/4.4/bin \
         /util/opt/singularity/2.2/gcc/4.4/bin \
         /uufs/chpc.utah.edu/sys/installdir/singularity/std/bin ; do
     if [ -e "$LOCATION" ]; then
         echo " ... prepending $LOCATION to PATH"
         export PATH="$LOCATION:$PATH"
         break
     fi
done
export PATH
HAS_SINGULARITY="False"
export OSG_SINGULARITY_VERSION=`singularity --version 2>/dev/null`
if [ "x$OSG_SINGULARITY_VERSION" != "x" ]; then
     HAS_SINGULARITY="True"
     export OSG_SINGULARITY_PATH=`which singularity`
else
     # some sites requires us to do a module load first - not sure if we always want to do that
     export OSG_SINGULARITY_VERSION=`module load singularity >/dev/null 2>&1; singularity --version 2>/dev/null`
     if [ "x$OSG_SINGULARITY_VERSION" != "x" ]; then
         HAS_SINGULARITY="True"
         export OSG_SINGULARITY_PATH=`module load singularity >/dev/null 2>&1; which singularity`
     fi
fi

# default image for this glidein
export OSG_SINGULARITY_IMAGE_DEFAULT="/cvmfs/singularity.opensciencegrid.org/bbockelm/cms:rhel6"

# for now, we will only advertise singularity on nodes which can access cvmfs
if [ ! -e "$OSG_SINGULARITY_IMAGE_DEFAULT" ]; then
     HAS_SINGULARITY="False"
fi

# Let's do a simple singularity test by echoing something inside, and then
# grepping for it outside. This takes care of some errors which happen "late"
# in the execing, like:
# ERROR  : Could not identify basedir for home directory path: /
if [ "x$HAS_SINGULARITY" = "xTrue" ]; then
     SINGULARITY_HOME=`mktemp -d`
     echo "$OSG_SINGULARITY_PATH exec --home $SINGULARITY_HOME:/srv --bind /cvmfs --pwd /srv --contain --ipc --pid $OSG_SINGULARITY_IMAGE_DEFAULT echo Hello World | grep Hello World"
     if ! ($OSG_SINGULARITY_PATH exec --home $SINGULARITY_HOME:/srv \
                                      --bind /cvmfs \
                                      --pwd /srv \
                                      --contain --ipc --pid \
                                      "$OSG_SINGULARITY_IMAGE_DEFAULT" \
                                      echo "Hello World" \
                                      | grep "Hello World") 1>&2 \
     ; then
         # singularity simple exec failed - we are done
	 /bin/rm -rf $SINGULARITY_HOME
	 echo $ERROR_FAILED_SINGULARITY_EXEC_MSG
         exit $ERROR_FAILED_SINGULARITY_EXEC
     fi
     /bin/rm -rf $SINGULARITY_HOME
     exit 0
fi
echo $ERROR_NO_SINGULARITY_MSG
exit $ERROR_NO_SINGULARITY
