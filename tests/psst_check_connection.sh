#!/bin/bash
#
# Description:
#     Script to test connectivity and check clock for skew
#

############################################################

PSST_ISSUE_TEXT="PSST_ISSUE_TEXT: "
exit_code=0

# -------------------------------------------------------- #
# function of connection test with each endpoint
# @argu: endpiont
# -------------------------------------------------------- #
check_connecction() {
  endpoint="$1"
  echo "Trying to get http header of ${endpoint}"
  echo "execute curl command:curl -4 -k --connect-timeout 5 --head -s https://${endpoint}"
  client_timestamp_sent=$(date +%s)
  header=$(curl -4 -k --connect-timeout 5 --head -s https://${endpoint})
  rc=$?
  client_timestamp_received=$(date +%s)
  echo "Connecting ended with exit code: " $rc
  if [ "$rc" -eq 0 ]; then
    echo "$header" | sed 's/\&//g' # removing illegal char & for xml
    server_time=$(echo "$header" | sed -n 's/.*Date: //p')
    server_timestamp=$(date --date="${server_time}" +%s)
    round_trip_time=$((client_timestamp_received-client_timestamp_sent))

    echo "Client timestamp sent: $client_timestamp_sent
    Server timestamp: $server_timestamp
    Client timestamp received: $client_timestamp_received
    Round trip time: $round_trip_time"

    one_way_trip=$((round_trip_time / 2))
    time_difference=$((client_timestamp_sent + one_way_trip - server_timestamp))
    if [ "$time_difference" -ge -60 -a "$time_difference" -le 60 ]; then

      echo "INFO: Skew with $endpoint is no bigger than 60 sec, [c_ts_sent:$client_timestamp_sent, c_ts_receive:$client_timestamp_received, s_ts:$server_timestamp]"
      PSST_ISSUE_TEXT+="INFO: Skew with $endpoint is no bigger than 60 sec, [c_ts_sent:$client_timestamp_sent, c_ts_receive:$client_timestamp_received, s_ts:$server_timestamp]\n"

      echo "Clock skew is not bigger than 60 sec"
      echo "Test was passed, exiting"
      return 0
    else

      echo "ERROR: skew with $endpoint is bigger than 60 sec, [c_ts_sent: $client_timestamp_sent, c_ts_receive:$client_timestamp_received, s_ts: $server_timestamp]"
      PSST_ISSUE_TEXT+="ERROR: skew with $endpoint is bigger than 60 sec, [c_ts_sent: $client_timestamp_sent, c_ts_receive:$client_timestamp_received, s_ts: $server_timestamp]\n"
      return $ERROR_CLOCK_SKEW
    fi
  else

    echo "ERROR: Failed to get http header of $endpoint" 
    PSST_ISSUE_TEXT+="ERROR: Failed to get http header of $endpoint\n"
    return $ERROR_NO_CONNECTION
  fi
}

# loop to test the conectivity with each endpoints 
endpoints="google.com cern.ch"
for hostname in $endpoints
do
  check_connecction $hostname
  rc=$?
  [[ $rc -ne 0 ]] && exit_code=$rc
  echo
done
echo $PSST_ISSUE_TEXT
exit $exit_code
