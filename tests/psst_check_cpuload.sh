#!/bin/bash
# 
# Description:
#    Script to check cpu load
#

############################################################
PSST_ISSUE_TEXT="PSST_ISSUE_TEXT: "
exit_code=0

# number of cores assigned to the pilot
pilot_cores=$1

echo "execute /usr/bin/uptime: $(/usr/bin/uptime)"
cpu_load=$(/usr/bin/uptime |awk -F'load average: |, ' '/load average/{print $5}')
echo "cpu load in last minite is: $cpu_load"

# number of cpus
echo "Number of physical CPUs: $(grep -c ^processor /proc/cpuinfo)"
physical_cpus=$(grep -c ^processor /proc/cpuinfo)

# cpu overload declares ERROR, cpu_load+pilot_cores overload declares WARNING 
if [[ $(awk '{if($1>=$2)print 1;else print 0}' <<<"$cpu_load $physical_cpus") -eq 1 ]]; then
  echo "INFO: cpu load is too high, [physical_cpus=${physical_cpus},cpu_load=${cpu_load}]"
  PSST_ISSUE_TEXT+="INFO: cpu load is too high, [physical_cpus=${physical_cpus},cpu_load=${cpu_load}]\n"
  echo $PSST_ISSUE_TEXT
  exit $ERROR_CPU_LOAD
elif [[ $(awk '{if($1+$2>=$3)print 1;else print 0}' <<<"$cpu_load $pilot_cores $physical_cpus") -eq 1 ]]; then
  echo "WARNING: CPU load of last minutes + pilot cores is higher than number of physical CPUs, [cload=$cpu_load,pltcores=$pilot_cores,phscpus=$physical_cpus]"
  PSST_ISSUE_TEXT+="WARNING: CPU load of last minutes + pilot cores is higher than number of physical CPUs, [cload=$cpu_load,pltcores=$pilot_cores,phscpus=$physical_cpus]\n"
  echo $PSST_ISSUE_TEXT
  exit $WARNING_CPU_LOAD
else
  echo "INFO: CPU load is OK, [cload=$cpu_load,pltcores=$pilot_cores,phscpus=$physical_cpus]"
  PSST_ISSUE_TEXT+="INFO: CPU load is OK, [cload=$cpu_load,pltcores=$pilot_cores,phscpus=$physical_cpus]\n"
  echo $PSST_ISSUE_TEXT
  exit 0
fi
