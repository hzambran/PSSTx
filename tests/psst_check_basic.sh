#!/bin/bash
#
# Description:
#   script to check basic info
#

############################################################
PSST_ISSUE_TEXT="PSST_ISSUE_TEXT: "
exit_code=0

# -------------------------------------------------------- # 
# required exit_code list
# -------------------------------------------------------- # 
ERROR_RUNNING_IN_SINGULARITY=119
ERROR_OUT_SYNC_NTP_TIME=120
ERROR_CVMFS_NOT_INSTALL=121
ERROR_CVMFS_DIR_NOT_ACCESSIBLE=123
# -------------------------------------------------------- # 
# check if running in container or virtual machine
# if running in container, get which type of container
# -------------------------------------------------------- # 
container="none"
if [[ -n "$SINGULARITY_CONTAINER" ]]; then
    container="singularity"
    container_version=`singularity --version`
elif grep docker /proc/1/cgroup -qa >/dev/null 2>&1 ; then
    container="docker"
    container_version=`docker version`
elif [[ -n "$SHIFTER" ]] && [[ -n "$SHIFTER_IMAGE" ]]; then
    container="shifter"
    container_version=`shifter --version`
fi

## check if running inside virtual machine (only use H vendor)
if [[ `lscpu | grep "^Hypervisor vendor"` ]];
then
    vm="virtual machine"
    vm_name=`cat /sys/devices/virtual/dmi/id/product_name`
fi

## running in singularity will cause error exit
if [[ $container == "singularity" ]]; then
    echo "ERROR: Test job is running in container: $container, $container_version ."
    PSST_ISSUE_TEXT+="ERROR: Test job is running in container, [$container, $container_version]."
    PSST_ISSUE_TEXT+="\n"
    exit_code=$ERROR_RUNNING_IN_SINGULARITY
    exit $exit_code
elif [[ $container == "docker" ]]; then
    echo "INFO: Test job is running in container: $container, container version is $container_version ."
    PSST_ISSUE_TEXT+="INFO: Test job is running in $container, version is $container_version ."
    PSST_ISSUE_TEXT+="\n"
elif [[ $container == "shifter" ]]; then
    echo "INFO: Test job is running in container: $container, container version is $container_version ."
    PSST_ISSUE_TEXT+="Test job is running in $container, version is $container_version ."
    PSST_ISSUE_TEXT+="\n"
elif [[ $vm=="virtual machine" ]]; then
    echo "INFO: Test job is running in virtual machine: $vm_name"
    PSST_ISSUE_TEXT+="INFO: Test job is running in virtual machine, product name is $vm_name"
    PSST_ISSUE_TEXT+="\n"
else
    echo "INFO: Test job is not running in container or virtual machine."
    PSST_ISSUE_TEXT+="INFO: Test job is not running in container or vm."
    PSST_ISSUE_TEXT+="\n"
fi

# -------------------------------------------------------- # 
# check for basic stuffs (cpu, mem, ntp time-sync, ulimit)
# -------------------------------------------------------- # 
cpu_number=`lscpu |awk '{if($1=="CPU(s):"){print $2}}'`
mem_total=`free -m | awk -F':||[ ]+' '/^Mem/{print $3}'`
mem_free=`free -m | awk -F':||[ ]+' '/^Mem/{print $5}'`
echo "INFO: CPU and MEM: [ncpu=$cpu_number, tmem=$mem_total MB, fmem=$mem_free MB]"
PSST_ISSUE_TEXT+="INFO: basics info, [ncpu=$cpu_number, tmem=$mem_total MB, fmem=$mem_free MB]\n"

sys_info=`uname -a`
sys_version=`cat /etc/system-release`
echo "INFO: System info: $sys_info; system release: $sys_version"
PSST_ISSUE_TEXT+="INFO: system info: $sys_info; system release: $sys_version\n" 

## compared with ntp pool link, https://www.ntppool.org/en/ 
throttle=300
local_time=$(date --utc)
local_st=$(date --utc +%s)
server_time=$(curl -4 -k --connect-timeout 5 --head -s https://www.ntppool.org/en/ |sed -n 's/.*Date: //p') # https://www.ntppool.org/en/
server_st=$(date --date="${server_time}" +%s)
offset=$(($server_st-$local_st))
if [[ $sign -lt $throttle ]];
then
    echo "INFO: The offset between local time and ntp pool time is less than $throttle s, [ltime=$local_time, offset=$offset]"
    PSST_ISSUE_TEXT+="INFO: The offset of ntp time is less than $throttle s, [ltime=$local_time, offset=$offset]"
    PSST_ISSUE_TEXT+="\n"
else
    echo "ERROR: The offset between local time and ntp pool time is more than $throttle s."
    PSST_ISSUE_TEXT+="ERROR: The offset of ntp time is more than $throttle s, [ltime=$local_time, offset=$offset]"
    PSST_ISSUE_TEXT+="\n"
    exit_code=$ERROR_OUT_SYNC_NTP_TIME
    exit $exit_code
fi

## get open files and max user processes from command ulimit
file_descriptor=`ulimit -a |awk -F '[ ]+\\\\(.+\\\\)[ ]+' '{if($1=="open files"){print $2}}'`
max_user_processes=`ulimit -a |awk -F '[ ]+\\\\(.+\\\\)[ ]+' '{if($1=="max user processes"){print $2}}'`
echo "INFO: Ulimit info: [file_descriptor=$file_descriptor, max_user_processes=$max_user_processes]"
PSST_ISSUE_TEXT+="INFO: Ulimit info: [file_descriptor=$file_descriptor, max_user_processes=$max_user_processes]"
PSST_ISSUE_TEXT+="\n"

# -------------------------------------------------------- # 
# basically check CVMFS installation, mount and access
# -------------------------------------------------------- # 
cvmfs_repository=("cms.cern.ch" "singularity.opensciencegrid.org" "oasis.opensciencegrid.org")
fs_installed="unknown"

# function is_nfs() {
#     local nfs_repo="$1"
#     [ "x`/usr/bin/stat -f -L -c %T $nfs_repo`" == "xnfs" ]
#     return $?
# }

function get_fs_type() {
    local cvmfs_repo="$1"
    /usr/bin/stat -f -L -c %T $cvmfs_repo
    return $?
}

function is_cvmfs() {
    local cvmfs_repo="$1"
    /usr/bin/cvmfs_config stat $1 >/dev/null
    return $?
}

function get_xattr() {
    local attr_name=$1
    local repo=$2
    result=`/usr/bin/attr -q -g $attr_name $repo` >/dev/null 2>&1 
    echo $result
    return $? 
}

## cvmfs version, check if the version is > 2.4.3
## 

function cvmfs_basic_check() {
    local mnt_repo=$1 
    local cvmfs_dir="/cvmfs/$mnt_repo"
    if [ -d $cvmfs_dir ]; then
	cd $cvmfs_dir; cd -
        # is_nfs $cvmfs_dir
	# rc_is_nfs=$?
	is_cvmfs $mnt_repo
	rc_is_cvmfs=$?
        if [ $rc_is_cvmfs -eq 0 ]; then
	    fs_installed="cvmfs"
	    echo "INFO: cvmfs directory $cvmfs_dir is mounted as CVMFS."
	    PSST_ISSUE_TEXT+="INFO: cvmfs directory $cvmfs_dir is mounted as CVMFS.\n"

	    # Read repository config
	    if [ -f /etc/cvmfs/config.sh ]; then
		echo "INFO: /etc/cvmfs/config.sh exists, excuting..."
		. /etc/cvmfs/config.sh
            else
		echo "NOTICE: /etc/cvmfs/config.sh doesn't exist."
		PSST_ISSUE_TEXT+="NOTICE: /etc/cvmfs/config.sh doesn't exist.\n"
	    fi

	    # Read cvmfs config
	    cvmfs_readconfig 
            if [ $? -ne 0 ]; then
	        echo "NOTICE: failed to read CernVM-FS configuration."
		PSST_ISSUE_TEXT+="NOTICE: failed to read CernVM-FS configuration.\n"
	    fi
	    
	    FQRN=`cvmfs_mkfqrn $mnt_repo`
	    ORG=`cvmfs_getorg $FQRN`
	    local cvmfs_repo=${CVMFS_MOUNT_DIR}/${FQRN}

	    # access the targeted cvmfs repository
	    cd "${cvmfs_repo}" && ls . >/dev/null
            if [ $? -ne 0 ]; then
                echo "ERROR: CVMFS_DIR is not accessible, [CVMFS_DIR=$cvmfs_repo]."
                PSST_ISSUE_TEXT+="ERROR: CVMFS_DIR is not accessible, [CVMFS_DIR=$cvmfs_repo]\n"
                echo $PSST_ISSUE_TEXT
                exit $ERROR_CVMFS_DIR_NOT_ACCESSIBLE
            else
                echo "INFO: CVMFS_DIR is accessible, [CVMFS_DIR=$cvmfs_repo]"
                PSST_ISSUE_TEXT+="INFO: CVMFS_DIR is accessible, [CVMFS_DIR=$cvmfs_repo]"
                PSST_ISSUE_TEXT+="\n"
            fi
	    
            vers_loaded=`get_xattr version $cvmfs_repo`
            if [ $? -ne 0 ]; then
                echo "WARNING: cannot retrieve attribute 'version' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="WARNING: cannot retrieve attribute 'version' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="\n"
	    fi

            vers_installed=`/usr/bin/cvmfs2 --version 2>&1 | awk '{print $NF}'`
            if [ $? -ne 0 ]; then
		echo "WARNING: cannot execute '/usr/bin/cvmfs2 --version'."
                PSST_ISSUE_TEXT+="WARNING: cannot execute '/usr/bin/cvmfs2 --version'."
                PSST_ISSUE_TEXT+="\n"
	    fi 

	    cvmfs_revision=`get_xattr revision $cvmfs_repo`
            if [ $? -ne 0 ]; then
		echo "WARNING: cannot retrieve attribute 'revision' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="WARNING: cannot retrieve attribute 'revision' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="\n"
	    fi 

	    cvmfs_nioerr=`get_xattr nioerr $cvmfs_repo`
            if [ $? -ne 0 ]; then
		echo "WARNING: cannot retrieve attribute 'nioerr' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="WARNING: cannot retrieve attribute 'nioerr' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="\n"
	    fi 

	    cvmfs_usedfd=`get_xattr usedfd $cvmfs_repo`
            if [ $? -ne 0 ]; then
		echo "WARNING: cannot retrieve attribute 'usedfd' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="WARNING: cannot retrieve attribute 'usedfd' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="\n"
	    fi 

	    cvmfs_maxfd=`get_xattr maxfd $cvmfs_repo`
            if [ $? -ne 0 ]; then
		echo "WARNING: cannot retrieve attribute 'maxfd' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="WARNING: cannot retrieve attribute 'maxfd' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="\n"
	    fi 

	    cvmfs_fdratio=$(($cvmfs_usedfd*100/$cvmfs_maxfd))
	    if [ $cvmfs_fdratio -gt 50 ]; then
		echo "NOTICE: the usage of file descriptors is more than 50%."
		PSST_ISSUE_TEXT+="NOTICE: the usage of file descriptors is more than 50%, [cvmfs_fdratio=$cvmfs_fdratio]."
		PSST_ISSUE_TEXT+="\n"
	    fi

	    cvmfs_nclg=`get_xattr nclg $cvmfs_repo`
            if [ $? -ne 0 ]; then
		echo "WARNING: cannot retrieve attribute 'maxfd' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="WARNING: cannot retrieve attribute 'maxfd' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="\n"
	    fi 

	    cvmfs_pid=`get_xattr pid $cvmfs_repo`
            if [ $? -ne 0 ]; then
		echo "WARNING: cannot retrieve attribute 'pid' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="WARNING: cannot retrieve attribute 'pid' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="\n"
	    fi 

	    cvmfs_timeout=`get_xattr timeout $cvmfs_repo`
            if [ $? -ne 0 ]; then
		echo "WARNING: cannot retrieve attribute 'timeout' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="WARNING: cannot retrieve attribute 'timeout' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="\n"
	    fi 

	    cvmfs_timeout_direct=`get_xattr timeout_direct $cvmfs_repo`
            if [ $? -ne 0 ]; then
		echo "WARNING: cannot retrieve attribute 'timeout_direct' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="WARNING: cannot retrieve attribute 'timeout_direct' for $cvmfs_repo."
                PSST_ISSUE_TEXT+="\n"
	    fi 
	    echo "NOTICE: CVMFS key variables for $cvmfs_repo, [vers_loaded=$vers_loaded, vers_installed=$vers_installed,
	    cvmfs_revision=$cvmfs_revision, cvmfs_nioerr=$cvmfs_revision, cvmfs_usedfd=$cvmfs_usedfd, 
	    cvmfs_maxfd=$cvmfs_maxfd, cvmfs_nclg=$cvmfs_nclg, cvmfs_pid=$cvmfs_pid, 
            cvmfs_timeout=$cvmfs_timeout, cvmfs_timeout_direct=$cvmfs_timeout_direct]. "

	    PSST_ISSUE_TEXT+="NOTICE: CVMFS key variables for $cvmfs_repo, [vers_loaded=$vers_loaded, vers_installed=$vers_installed, 
	    cvmfs_revision=$cvmfs_revision, cvmfs_nioerr=$cvmfs_revision, cvmfs_usedfd=$cvmfs_usedfd, 
	    cvmfs_maxfd=$cvmfs_maxfd, cvmfs_nclg=$cvmfs_nclg, cvmfs_pid=$cvmfs_pid, 
            cvmfs_timeout=$cvmfs_timeout, cvmfs_timeout_direct=$cvmfs_timeout_direct]."
	    PSST_ISSUE_TEXT+="\n"
        # elif [ $rc_is_nfs -eq 0 ]; then 
        #     fs_installed="nfs"	    
	#     echo "INFO: cvmfs directory $cvmfs_dir is mounted as NFS."
	#     PSST_ISSUE_TEXT+="INFO: cvmfs directory $cvmfs_dir is mounted as NFS.\n"
	#     /usr/sbin/nfsstat -m $cvmfs_dir
        else
	    fs_type=$(get_fs_type $cvmfs_dir)
	    echo "ERROR: CVMFS is not installed." 
	    PSST_ISSUE_TEXT+="ERROR: CVMFS is not installed.\n"
	    echo $PSST_ISSUE_TEXT
	    exit $ERROR_CVMFS_NOT_INSTALL
	fi
    fi
}

## Quota (vienna, rome)
## no cvmfs but dir exists 

for REPO in ${cvmfs_repository[@]} ;
do
    echo "Check $REPO ----------------------------------------------------------"
    cvmfs_basic_check $REPO
    echo "End check $REPO ------------------------------------------------------"
done

if [ "$fs_installed"=="cvmfs" ]; then
    echo "INFO: CVMFS rpms info -----------------------"
    /bin/rpm -qa |grep cvmfs
    echo "---------------------------------------------"
fi

# -------------------------------------------------------- # 
# IO state (better solution in the future) 
# -------------------------------------------------------- # 

############################################################
if [[ "$PSST_ISSUE_TEXT" == "PSST_ISSUE_TEXT: " ]]; then
    PSST_ISSUE_TEXT+="INFO: Basic check is OK.\n"
fi
echo $PSST_ISSUE_TEXT
exit $exit_code
